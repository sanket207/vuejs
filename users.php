<!DOCTYPE html>
<html>
<head>
	<title>Users</title>
	<script type="text/javascript" src="js/vue.js"></script>
	<script type="text/javascript" src="js/axios.min.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>
	<div id="main">

		<form id="usersForm">
			<div>
				<label>First Name</label>
				<input type="text" name="first_name">
			</div>
			<div>
				<label>Last Name</label>
				<input type="text" name="last_name">
			</div>
			<div>
				<label>Email</label>
				<input type="text" name="email">
			</div>
			<div>
				<button type="button" v-on:click="userdata">Submit</button>	
			</div>
			
		</form>
	</div>


	<script type="text/javascript">
		var v = new Vue({

			el : '#main',
			methods:
			{
				userdata : function(event)
				{
					/*axios.get('http://localhost/vue/usersdata.php').then(function(response){
						console.log(response);
					});*/

					/*var params = new URLSearchParams();
					params.append('name', 'sanket');
					params.append('age', 10);*/

					var form_data = new FormData($('#usersForm')[0]);


					axios.post('http://localhost/vue/usersdata.php',form_data).then(function(response){

						console.log(response);
					});

					//console.log(form_data);
				}
			}
		});	


	</script>


</body>
</html>