<!DOCTYPE html>
<html>
<head>
	<title>Index</title>
	<script type="text/javascript" src="js/vue.js"></script>
</head>
<body>
	<div id="theDiv" style="text-align: center;">
		<h3>{{ message }}</h3>
		<div>
			<label>First Number </label>
			<input type="text" name="first_num" v-model="first_num">
		</div>

		<div>
			<label>Second Number</label>
			<input type="text" name="second_num" v-model = "second_num">
		</div>
		<div>
			<label>Operator</label>
			<input type="text" name="operator" v-model = "operator">
		</div>
		<div v-html="htmlcontent">
			<!-- <label>Result</label>
			<input type="text" name="result" v-model = "result"> -->
		</div>
		<div>
			<button type="button" v-on:click="displayresult">Submit</button>
		</div>

	</div>

	<script type="text/javascript">
		
		var vue_obj = new Vue({

			el : '#theDiv',
			data :
			{
				message : 'Hello User',
				first_num : '',
				second_num : '',
				operator : '',
				result : '',
				tmp : '',
				res : '',
				htmlcontent : ''
			},
			methods :
			{
				displayresult : function(event)
				{
					this.first_num = parseInt(this.first_num);
					this.second_num = parseInt(this.second_num);
					this.tmp = '<label> Result </label>';
					console.log(this.res);
					if('+' == this.operator)
					{
						this.result = (this.first_num + this.second_num);
						this.tmp = this.tmp + '<input type="text" name="result" v-model = "res" value = "'+this.result+'"><span><a href="javascript:void(0)" v-on:click="getValue">value</a></span>';
					}
					else if('-' == this.operator)
					{
						this.result = (this.first_num - this.second_num);
						this.tmp = this.tmp + '<input type="text" name="result" v-model="res" value = "'+this.result+'"> <span><a href="javascript:void(0)" v-on:click="getValue">value</a></span>';	
					}
					this.htmlcontent = this.tmp;

					console.log(this.htmlcontent);
					return this.htmlcontent;
				},
				getValue : function(event)
				{
					console.log(this.res);
				}
			},

		});

	</script>

</body>
</html>