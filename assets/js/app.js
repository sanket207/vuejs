var v = new Vue({

	el : "#mainDiv",
	data :
	{
		items : [],
		form: {
		  first_name : '',
		  last_name : '',
          email: '',
          user_name: '',
          form_action : 'save_user'
        },
        show: true
	},
	beforeMount : function(response)
	{
		//var self = this;
		var params = new URLSearchParams();
		params.append('action', 'get_users');
		
		axios.post(base_url+'/usersdata.php',params).then(response => {

			this.items = response.data.result;

		});
	},
	methods:
	{
		addUserData: function(event)
		{
			var form = document.getElementById('addUserForm');
			var form_data = new FormData(form);
			axios.post(base_url+'/usersdata.php',form_data).then(response => {

				console.log(response);
			});			
		}
	}



});