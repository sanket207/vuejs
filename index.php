<?php include('includes/header.php') ?>
<div class="container">

	
	<legend>
		Users 
		
	</legend>
	<div align="right">
		<!-- <button class="btn btn-primary">Add</button> -->
		<b-button v-b-modal.modal-1>Add</b-button>
	</div>

	<hr> 

	<table class="table table-striped">
		<head>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Username</th>
				<th>Email</th>
			</tr>
		</head>
		<tbody>
			<tr v-for="item in items">
				<td>{{ item.first_name }}</td>
				<td>{{ item.last_name }}</td>
				<td>{{ item.username }}</td>
				<td>{{ item.email }}</td>
			</tr>
		</tbody>
	</table>


	<b-modal id="modal-1" title="Add User" hide-footer="true">
    	<template>
		  <div>
		    <b-form  @reset="" id="addUserForm">
		    	<b-form-input
		          name="action"
		          type="hidden"
		          v-model="form.form_action"
		          ></b-form-input>


		      <b-form-group id="first_name" label="First Name" label-for="first-name">
		        <b-form-input
		          id="input-1"
		          name = 'first_name'
		          v-model="form.first_name"
		          type="text"
		          required
		          placeholder="First Name"
		        ></b-form-input>
		      </b-form-group>

		      <b-form-group id="last_name" label="Last Name" label-for="last-name">
		        <b-form-input
		          id="last-name"
		          name="last_name"
		          v-model="form.last_name"
		          required
		          placeholder="Last Name"
		        ></b-form-input>
		      </b-form-group>

		      <b-form-group id="user_email" label="Email" label-for="user-email">
		        <b-form-input
		          id="user-email"
		          name="email"
		          v-model="form.email"
		          required
		          placeholder = "Email Address"
		        ></b-form-input>
		      </b-form-group>

		      <b-form-group id="user_name" label="User Name" label-for="user-name">
		        <b-form-input
		          id="user-name"
		          name="user_name"
		          v-model="form.user_name"
		          required
		          placeholder = "User Name"
		        ></b-form-input>
		      </b-form-group>

		      <b-button type="button" @click.prevent="addUserData()" variant="primary">Submit</b-button>
		      <b-button type="reset" variant="danger">Reset</b-button>
		    </b-form>
		   
		  </div>
		</template>
  	</b-modal>

</div>

<?php include('includes/footer.php') ?>